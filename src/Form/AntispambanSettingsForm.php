<?php

namespace Drupal\antispamban\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AntispambanSettingsForm.
 */
class AntispambanSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'antispamban.antispambansettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'antispamban_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('antispamban.antispambansettings');


    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key for language detection https://detectlanguage.com'),
      '#default_value' => $config->get('api_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('antispamban.antispambansettings')->set('api_key',$form_state->getValue('api_key'))
      ->save();
  }

}
