<?php

namespace Drupal\antispamban\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Anti spam ban entity entity.
 *
 * @ConfigEntityType(
 *   id = "anti_spam_ban_entity",
 *   label = @Translation("Anti spam ban entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\antispamban\AntiSpamBanEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\antispamban\Form\AntiSpamBanEntityForm",
 *       "edit" = "Drupal\antispamban\Form\AntiSpamBanEntityForm",
 *       "delete" = "Drupal\antispamban\Form\AntiSpamBanEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\antispamban\AntiSpamBanEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "anti_spam_ban_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/anti_spam_ban_entity/{anti_spam_ban_entity}",
 *     "add-form" = "/admin/structure/anti_spam_ban_entity/add",
 *     "edit-form" = "/admin/structure/anti_spam_ban_entity/{anti_spam_ban_entity}/edit",
 *     "delete-form" = "/admin/structure/anti_spam_ban_entity/{anti_spam_ban_entity}/delete",
 *     "collection" = "/admin/structure/anti_spam_ban_entity"
 *   }
 * )
 */
class AntiSpamBanEntity extends ConfigEntityBase implements AntiSpamBanEntityInterface {

  /**
   * The Anti spam ban entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Anti spam ban entity label.
   *
   * @var string
   */
  protected $label;



}
