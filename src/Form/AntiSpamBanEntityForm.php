<?php

namespace Drupal\antispamban\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class AntiSpamBanEntityForm.
 */
class AntiSpamBanEntityForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $anti_spam_ban_entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $anti_spam_ban_entity->label(),
      '#description' => $this->t("Label for the Anti spam ban entity."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $anti_spam_ban_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\antispamban\Entity\AntiSpamBanEntity::load',
      ],
      '#disabled' => !$anti_spam_ban_entity->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $anti_spam_ban_entity = $this->entity;
    $status = $anti_spam_ban_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage(
            $this->t(
              'Created the %label Anti spam ban entity.', [
                '%label' => $anti_spam_ban_entity->label(),
              ]
            )
          );
        break;

      default:
        $this->messenger()
          ->addMessage(
            $this->t(
              'Saved the %label Anti spam ban entity.', [
                '%label' => $anti_spam_ban_entity->label(),
              ]
            )
          );
    }
    $form_state->setRedirectUrl($anti_spam_ban_entity->toUrl('collection'));
  }

}
