<?php

namespace Drupal\antispamban\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Anti spam ban entity entities.
 */
interface AntiSpamBanEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
