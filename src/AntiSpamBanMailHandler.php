<?php


namespace Drupal\antispamban;


use Drupal\contact\MailHandler;
use Drupal\contact\MessageInterface;
use Drupal\Core\Session\AccountInterface;

class AntiSpamBanMailHandler extends MailHandler {

  public function sendMailMessages(MessageInterface $message, AccountInterface $sender) {
    if (\Drupal::currentUser()->id() > 0) {
      parent::sendMailMessages($message,$sender);
    }

  }

  public function forceSendMailMessage(MessageInterface $message,AccountInterface $sender){
  parent::sendMailMessages($message,$sender);
  }
}
