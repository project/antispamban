<?php


namespace Drupal\antispamban;


use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

class AntispambanServiceProvider extends  ServiceProviderBase implements ServiceProviderInterface {

  public function alter(ContainerBuilder $container) {
$definition = $container->getDefinition("contact.mail_handler");

$definition->setClass('Drupal\antispamban\AntiSpamBanMailHandler');

   // parent::alter($container); // TODO: Change the autogenerated stub
  }

}
