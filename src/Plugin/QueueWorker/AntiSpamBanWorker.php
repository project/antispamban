<?php

namespace Drupal\antispamban\Plugin\QueueWorker;

use Drupal\user\Entity\User;
use DetectLanguage\DetectLanguage;
use Drupal;
use Drupal\antispamban\Entity\AntiSpamBanEntity;
use Drupal\contact\Entity\Message;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Queue\RequeueException;
use Drupal\Driver\Exception\Exception;
use Drupal\field\Entity\FieldConfig;

/**
 * Updates a feed's items.
 *
 * @QueueWorker(
 *   id = "antispamban_worker",
 *   title = @Translation("AntiSpamBan worker"),
 *   cron = {"time" = 10}
 * )
 */
class AntiSpamBanWorker extends QueueWorkerBase {

const MESSAGE_SPAM = 2;
const MESSAGE_DRAFT = 0;
const MESSAGE_OK = 1;


  /**
   * {@inheritdoc}
   */
  public function processItem($data) {

    // Php -R gethostbyname("57.24.56.83.dnsbl.spfbl.net");
    // IP CHECK.
    $message_entity = Message::load($data['mid']);
    $anti_entities = AntiSpamBanEntity::loadMultiple(NULL);
    $target_ip = $message_entity->get('ip_address')->value;

    $is_reliable = NULL;
    $message_confidence = NULL;
    $check_message = FALSE;
    $count = 0;
    $rev = array_reverse(explode('.', $target_ip));
    foreach ($anti_entities as $entity) {
      $lookup = implode('.', $rev) . '.' . $entity->get('label');
      if ($lookup != gethostbyname($lookup)) {
        $count++;
      }

    }
    if ($count >= 3) {
      $message_entity->set('verification_status', MESSAGE_SPAM);
    }
    else {
      $check_message = TRUE;
    }

    if ($check_message) {
      $module_key = \Drupal::config('antispamban.antispambansettings')
        ->get('api_key');
      try {
        DetectLanguage::setApiKey($module_key);
        // DetectLanguage::setSecure(true);
        $languages_in_site = array_keys(
        Drupal::languageManager()
          ->getLanguages()
        );
        $api_status = DetectLanguage::getStatus();

        if ($api_status) {
          $limit = $api_status->daily_requests_limit;
          $message_spam = FALSE;
          if ($api_status->requests < $limit) {
            $fields_definitions = $message_entity->getFieldDefinitions();

            foreach ($fields_definitions as $key => $value) {
              $field_setting = ($value instanceof FieldConfig) ? $value->getThirdPartySetting('antispamban', 'validate_spam_field') : NULL;
              if ($field_setting === 1) {
                $field_value = $message_entity->get($key)->value;

                $language_response = DetectLanguage::detect($field_value);
                $is_reliable = $language_response[0]->isReliable;
                $message_confidence = $language_response[0]->confidence;
                $language = !empty($language_response) ? $language_response[0]->language : NULL;
                if ($language && (!in_array($language, $languages_in_site) || ($message_confidence < 2))) {
                  $message_spam = TRUE;
                  break;
                }

              }
            }

            if ($message_spam) {
              $message_entity->set('verification_status', MESSAGE_SPAM);
            }
            else {
              $message_entity->set('verification_status', MESSAGE_OK);
              $user = User::load(0);
              Drupal::service('contact.mail_handler')->forceSendMailMessage($message_entity, $user);
            }
          }
          else {
            $message = t('Request limit exceded');
            Drupal::logger('antispamban')->error($message);
            throw new RequeueException();
          }

        }

      }
      catch (Exception $e) {

        Drupal::logger('antispamban')->error($e->getMessage());
        throw new RequeueException();
      }
    }

    // TODO Boolean reliable.
    $message_entity->set('message_confidence', $message_confidence);
    $message_entity->set('is_reliable', $is_reliable ? 'true' : 'false');
    $message_entity->save();

  }

}
